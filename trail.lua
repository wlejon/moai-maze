Trail = {}
Trail.__index = Trail

function Trail.create(texture)
    local t = {}
    t.texture = texture
    setmetatable(t, Trail)
    return t
end

function Trail:point(node)
    self.trailQuad = MOAIGfxQuad2D.new()
    self.trailQuad:setTexture(self.texture)
    self.trailQuad:setRect(0, 0, 16, 16)
    self.trailProp = MOAIProp2D.new()
    self.trailProp:setDeck(self.trailQuad)
    self.trailProp:setLoc(8, 8)
    self.trailProp:setParent(node.transform)

    return self
end

function Trail:connectNodes(startNode, endNode)
    if (startNode == nil or endNode == nil) then
        return self
    end

    local x, y = startNode.transform:getLoc()
    local cx, cy = endNode.transform:getLoc()
    if x == cx and y == cy then
        return self
    end
    self.trailQuad = MOAIGfxQuad2D.new()
    self.trailQuad:setTexture(self.texture)
    self.trailQuad:setRect(0, 0, 4, 32)
    self.trailProp = MOAIProp2D.new()
    self.trailProp:setDeck(self.trailQuad)
    self.trailProp:setParent(startNode.transform)

    if x < cx then
        -- right
        self.trailProp:setLoc(48, 16)
        self.trailProp:setRot(90)
    elseif y < cy then
        -- top
        self.trailProp:setLoc(16, 16)
    elseif x > cx then
        -- left
        self.trailProp:setLoc(16, 16)
        self.trailProp:setRot(90)
    elseif y > cy then
        -- bottom
        self.trailProp:setLoc(16, -16)
    end

    return self
end

function Trail:render(layer)
--    if (slow) then
--        self.timeout = self.timeout + 0.05
--    end
--    self.action = self.trailProp:move(0, 0, 0, 0, 0, self.timeout)
--    local prop = self.trailProp
--    local item = self
--    self.action:setListener(MOAIAction.EVENT_STOP, function ()
      layer:insertProp(self.trailProp)
--    end)
end