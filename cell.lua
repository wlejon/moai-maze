Cell = {}
Cell.__index = Cell
function Cell.create()
    local cell = {}
    cell.neighbors = {}
    cell.attachedNeighbors = {}
    cell.visited = false
    cell.transform = MOAITransform2D.new()
    setmetatable(cell, Cell)
    return cell
end

function Cell:addNeighbor(cell)
    table.insert(self.neighbors, cell)
end

function Cell:getUnVisitedNeighbors()
    local neighbors = {}
    for i = 1, # self.neighbors do
        if self.neighbors[i].visited == false then
            table.insert(neighbors, self.neighbors[i])
        end
    end

    return neighbors
end

function Cell:getAttachedUnVisitedNeighbors()
    local neighbors = {}
    for i = 1, # self.attachedNeighbors do
        if self.attachedNeighbors[i].visited == false then
            table.insert(neighbors, self.attachedNeighbors[i])
        end
    end

    return neighbors
end

function Cell:removeWall(cell)
    local x, y = self.transform:getLoc()
    local cx, cy = cell.transform:getLoc()
    if x < cx then
        -- right
        layer:removeProp(self.rightProp)
    elseif y < cy then
        -- top
        layer:removeProp(self.topProp)
    elseif x > cx then
        -- left
        layer:removeProp(self.leftProp)
    elseif y > cy then
        -- bottom
        layer:removeProp(self.bottomProp)
    end
    table.insert(self.attachedNeighbors, cell)
end

function Cell:renderWalls()
    self.leftQuad = MOAIGfxQuad2D.new()
    self.leftQuad:setTexture(imageWall)
    self.leftQuad:setRect(0, 0, 4, 32)
    self.leftProp = MOAIProp2D.new()
    self.leftProp:setDeck(self.leftQuad)
    self.leftProp:setLoc(0, 0)
    self.leftProp:setParent(self.transform)
    layer:insertProp(self.leftProp)

    self.rightQuad = MOAIGfxQuad2D.new()
    self.rightQuad:setTexture(imageWall)
    self.rightQuad:setRect(0, 0, 4, 32)
    self.rightProp = MOAIProp2D.new()
    self.rightProp:setDeck(self.rightQuad)
    self.rightProp:setLoc(32, 0)
    self.rightProp:setParent(self.transform)
    layer:insertProp(self.rightProp)

    self.topQuad = MOAIGfxQuad2D.new()
    self.topQuad:setTexture(imageWall)
    self.topQuad:setRect(0, 0, 4, 32)
    self.topProp = MOAIProp2D.new()
    self.topProp:setDeck(self.topQuad)
    self.topProp:setLoc(32, 32)
    self.topProp:setRot(90)
    self.topProp:setParent(self.transform)
    layer:insertProp(self.topProp)

    self.bottomQuad = MOAIGfxQuad2D.new()
    self.bottomQuad:setTexture(imageWall)
    self.bottomQuad:setRect(0, 0, 4, 32)
    self.bottomProp = MOAIProp2D.new()
    self.bottomProp:setDeck(self.bottomQuad)
    self.bottomProp:setLoc(32, 0)
    self.bottomProp:setRot(90)
    self.bottomProp:setParent(self.transform)
    layer:insertProp(self.bottomProp)
end

function Cell:setPosition(x, y)
    self.transform:setLoc(x, y)
end