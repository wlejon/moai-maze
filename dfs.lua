
function dfsSearch(startCell, endCell)
    if startCell == endCell then
        return {}, {}
    end
    local currentCell = startCell
    local stack = {}
    local found = false
    local ex, ey = endCell.transform:getLoc()
    currentCell.visited = true
    while found == false do
        local neighbors = currentCell:getAttachedUnVisitedNeighbors()
        if #neighbors > 0 then
            table.insert(stack, currentCell)
            local neighborCell = neighbors[math.random(1, #neighbors)]
            neighborCell.visited = true
            local t =  Trail.create(imageTrail)
            t:connectNodes(currentCell, neighborCell)
            table.insert(trail, t)
            currentCell = neighborCell
        else
            if #stack == 0 then
                break;
            end
            currentCell = table.remove(stack)
        end
        local x, y = currentCell.transform:getLoc()
        if (x == ex and y == ey) then
            found = true
        end
    end
    if #stack > 0 then
        table.insert(stack, endCell)
    end

    return stack
end

function dfsCreate(startCell, gridCount)
    local currentCell = startCell
    local stack = {}
    local visited = 1
    currentCell.visited = true
    while visited < gridCount do
        local neighbors = currentCell:getUnVisitedNeighbors()
        if (# neighbors > 0) then
            table.insert(stack, currentCell)
            local neightborCell = neighbors[math.random(1, #neighbors)]
            visited = visited + 1
            neightborCell.visited = true
            neightborCell:removeWall(currentCell)
            currentCell:removeWall(neightborCell)
            currentCell = neightborCell
        else
            currentCell = table.remove(stack)
        end
    end
end
