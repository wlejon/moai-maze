require("cell")
require("trail")
require("dfs")
require("assets")

-- open a window (not needed on mobile platforms)
screenWidth = 1024;
screenHeight = 768;
MOAISim.openWindow("Maze", screenWidth, screenHeight)

-- create a viewport
viewport = MOAIViewport.new()
viewport:setSize(screenWidth, screenHeight)
viewport:setScale(screenWidth, screenHeight)

timeout = 0.025

-- create a layer and sets its viewport
layer = MOAILayer2D.new()
layer:setViewport(viewport)

trailLayer = MOAILayer2D.new()
trailLayer:setViewport(viewport)

startCell = nil

imageWidth, imageHeight = imageWall:getSize()

cellWidth = math.max(imageWidth, imageHeight)
cellHeight = math.max(imageWidth, imageHeight)
xCount = screenWidth/ cellWidth
yCount = screenHeight/ cellHeight
transform = MOAITransform2D.new()
-- setup our grid for dfs usage
grid = {}
trail = {}
previousRow = {}
for y = math.floor(-yCount / 2), math.floor(yCount / 2) do
    local currentRow = {}
    local previousCell = false
    for x = math.floor(-xCount / 2), math.floor(xCount / 2) do
        local cell = Cell.create()
        cell:setPosition(x * cellWidth, y * cellHeight)
        cell:renderWalls()
        cell.transform:setParent(transform)
        table.insert(grid, cell)
        table.insert(currentRow, cell)

        if (previousCell) then
            cell:addNeighbor(previousCell)
            previousCell:addNeighbor(cell)
        end

        -- set our initial start cell to the center of the maze
        if (y == 0 and x == 0) then
            startCell = cell;
        end

        previousCell = cell
    end
    if (# previousRow > 0) then
        for i = 1, # previousRow do
            previousRow[i]:addNeighbor(currentRow[i])
            currentRow[i]:addNeighbor(previousRow[i])
        end
    end

    previousRow = currentRow
end
-- zoom out a tad
transform:setScl(0.8, 0.8)

dfsCreate(grid[math.random(1, # grid)], # grid)
function clearVisited (grid)
    -- reset our visited states
    -- TODO review options for event based callbacks
    for i = 1, #grid do
        grid[i].visited = false
    end
end

function renderNode(node, texture)
    if node == nil then
        return
    end
    table.insert(trail,
        Trail.create(texture)
             :point(node)
    )
end

function renderStartNode(node)
    renderNode(node, imageTypeStart)
end

function renderEndNode(node)
    renderNode(node, imageTypeEnd)
end

timer = MOAITimer.new()
timer:setSpan(timeout)
timer:setMode(MOAITimer.LOOP)
timer:setListener(MOAITimer.EVENT_TIMER_LOOP,
    function ()
        if #trail > 0 then
            local cell = table.remove(trail, 1)
            cell:render(trailLayer)
        end
    end
)
timer:start()

MOAIInputMgr.device.mouseLeft:setCallback(
    function(isMouseDown)
        if(isMouseDown) then
            local x, y = MOAIInputMgr.device.pointer:getLoc()
            x, y = layer:wndToWorld(x,y)
            local sx, sy = transform:getScl()
            x = x / sx
            y = y / sy
            local mx = screenWidth*screenHeight
            local my = screenWidth*screenHeight
            local cell = grid[#grid]
            for i = 1, #grid do
                local gx, gy = grid[i].transform:getLoc()
                local gmx = math.abs(x - gx - 16)
                local gmy = math.abs(y - gy - 16)
                if gmx <= mx and gmy <= my then
                    cell = grid[i]
                    mx = gmx
                    my = gmy
                end
            end
            clearVisited(grid, true)
            trail = {}
            trailLayer:clear()
            renderStartNode(startCell)
            renderEndNode(cell)
            local stack = dfsSearch(startCell, cell)
            if (stack ~= nil and #stack > 1) then
                for i = 1, #stack do
                    local cell = stack[i]
                    local neighbor = stack[i+1]
                    if (cell == nil or neighbor == nil) then
                        break
                    end
                    local t = Trail.create(imageFinalPath)
                    t:connectNodes(cell, neighbor)
                    table.insert(trail, t)
                end
            end
            startCell = cell
        end
    end
)

MOAIGfxDevice.setClearColor(1,1,1,1)

-- push the layer onto the render stack
MOAISim.pushRenderPass(layer)
MOAISim.pushRenderPass(trailLayer)